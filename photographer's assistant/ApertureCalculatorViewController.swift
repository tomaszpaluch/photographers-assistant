//
//  TestClass1.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 28.02.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class BaseCalculatorController: UIViewController {
    var parallelAuxiliaryController: BaseCalculatorController!
    var controller: ViewController!

    func setActualShootingDistance(distance: Int) {}
    
    func calculateDepthOfField() {}
    
    func updateView(aperture: Aperture, and depth: Int) {}
    
    func setCalculationValues(beforeObject: Double, behindObject: Double, andHyperfocal hyperfocal: Double) {
        controller.setCalculationValues(beforeObject: beforeObject, behindObject: behindObject, andHyperfocal: hyperfocal)
    }
    
    func updateSecondAuxiliaryView(aperture: Aperture, and totalDepth: Int) {
        parallelAuxiliaryController.updateView(aperture: aperture, and: totalDepth)
    }
}
