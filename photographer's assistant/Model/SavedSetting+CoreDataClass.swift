//
//  SavedSetting+CoreDataClass.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 19.03.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//
//

import Foundation
import CoreData


public class SavedSetting: NSManagedObject {
    var medium: Medium!
    
    func addFocalLength(focalLength: Int) {
        focalLengths.append(focalLength)
        focalLengths.sort()
    }
    
    func removeFocalLength(index: Int) {
        focalLengths.remove(at: index)
    }
}
