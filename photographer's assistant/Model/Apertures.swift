//
//  Aperture.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 22.02.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

typealias Aperture = Double

class Apertures: NSObject {

    var apertureList: [Aperture]
    
    override init() {
        apertureList = []
        
        super.init()
        
        populateApertureList()
    }
    
    private func populateApertureList() {
        apertureList = [ 1.0, 1.1, 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.5, 2.8, 3.2, 3.5, 4, 4.5, 5.0, 5.6, 6.3, 7.1, 8, 9, 10, 11, 13, 14, 16, 18, 20, 22, 25, 29, 32, 36, 40, 45, 51, 57, 64, 72, 80, 90];
    }
    
    func getAperturesCount() -> Int {
        return apertureList.count
    }
    
    func getMaxAperture() -> Aperture {
        return apertureList[0]
    }
    
    func getMinAperture() -> Aperture {
        return apertureList.last!
    }
    
    func getAperture(atIndex index: Int) -> Aperture {
        return apertureList[index]
    }
    
    func getAperturesIndex(aperture: Aperture) -> Int {
        return apertureList.firstIndex(of: aperture) ?? apertureList.count - 1
    }
    
    func getNextClosesApertureTo(roughAperture: Double) -> Aperture? {
        return apertureList.first(where: {$0 >= roughAperture}) ?? calculateAperture(roughAperture: roughAperture)
    }
    
    func getNextApertureTo(roughAperture: Double) -> Aperture {
        return apertureList.first(where: {$0 > roughAperture}) ?? calculateAperture(roughAperture: roughAperture)
    }
    
    private func calculateAperture(roughAperture: Double) -> Aperture {
        var aperture = apertureList.last!
        let multiplier = sqrt(2.0)
        
        repeat {
            aperture *= multiplier
        }
        while aperture <= roughAperture
                
        return aperture
    }
}
