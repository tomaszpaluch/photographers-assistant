//
//  SavedSetting+CoreDataProperties.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 19.03.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//
//

import Foundation
import CoreData


extension SavedSetting {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SavedSetting> {
        return NSFetchRequest<SavedSetting>(entityName: "SavedSetting")
    }

    @NSManaged public var cameraName: String
    @NSManaged public var mediumID: Int16
    @NSManaged public var focalLengths: [Int]

}
