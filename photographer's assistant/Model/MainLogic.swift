//
//  SuperLogic.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 12.04.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class MainLogic: NSObject {
    //var viewController: MainViewController!
    var apertures: Apertures!
    var actualSetting: ActualSetting!
    
    public var setFocalLengthEquivalentLabel: ((Int?) -> Void)!
    public var setFieldOfLookingLabel: ((Double, Double) -> Void)!
    
    var actualShootingDistance: Int {
        didSet {
            calculateFieldOfView()
        }
    }
    
    init(actualSetting: ActualSetting, apertures: Apertures) {
        actualShootingDistance = 0
        
        super.init()
        
        //self.viewController = viewController
        self.apertures = apertures
        self.actualSetting = actualSetting
    }
    
    func makeCalculations() {
        if (canCalculateLensEquivalent()) {
            calculate35mmEquivalentValue()
        }
        
        calculateFieldOfView()
    }
    
    private func canCalculateLensEquivalent() -> Bool {
        return actualSetting.cameraSetting?.medium != nil
    }
    
    private func calculate35mmEquivalentValue() {
        let cropFactor = actualSetting.cameraSetting!.medium.calculateCropFactor()
        
        var equivalentValue: Int?
        
        if let actualFocalLength = actualSetting.actualFocalLength {
            equivalentValue = Int(Double(actualFocalLength) * cropFactor)
        }
        
        setFocalLengthEquivalentLabel(equivalentValue)
    }

    private func calculateFieldOfView() {
        let horizontalAngle = calculateHorizontalViewAngle()
        let verticalngle = calculateVerticalViewAngle()
        
        let width = 2 * sin(horizontalAngle) * ((Double)(actualShootingDistance) / 100)
        let height = 2 * sin(verticalngle) * ((Double)(actualShootingDistance) / 100)
        
        setFieldOfLookingLabel(width, height)
    }
    
    private func calculateHorizontalViewAngle() -> Double {
        if (canCalculateViewAngle()) {
            let width = actualSetting.cameraSetting!.medium.width / 100
            let focalLength = (Double)(actualSetting.actualFocalLength!) / 1000
            
            return atan(width / 2 / focalLength)
        }
        return 0
    }
    
    private func calculateVerticalViewAngle() -> Double {
        if (canCalculateViewAngle()) {
            let height = actualSetting.cameraSetting!.medium.height / 100
            let focalLength = (Double)(actualSetting.actualFocalLength!) / 1000
            
            return atan(height / 2 / focalLength)
        }
        return 0
    }
    
    private func canCalculateViewAngle() -> Bool {
        return actualSetting.cameraSetting != nil && actualSetting.actualFocalLength != nil
    }
}
