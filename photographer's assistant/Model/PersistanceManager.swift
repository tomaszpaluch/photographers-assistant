//
//  PersistanceManager.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 19.03.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import Foundation
import CoreData

final class PersitanceManager {
    private init() {}
    static let shared = PersitanceManager()
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "photographer_s_assistant")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var context = persistentContainer.viewContext
    
    func fetch() -> [SavedSetting] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SavedSetting")
        
        do {
            let fetchedObjects = try context.fetch(fetchRequest) as? [SavedSetting]
            return fetchedObjects ?? [SavedSetting]()
        } catch {
            print(error)
            return [SavedSetting]()
        }
    }
    
    func delete(_ object: NSManagedObject) {
        context.delete(object)
        saveContext()
    }
    
    func saveContext () {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
