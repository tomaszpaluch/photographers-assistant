//
//  Settings.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 12.04.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class Settings: NSObject {
    //var settingViewController: SettingsViewController!
    var actualSetting: ActualSetting!
    
    var persitanceManager: PersitanceManager
    
    var settingsList: [SavedSetting]
    var mediumList: [Medium]
    
    var populatePicker: ((String, [Medium]) -> ())!
    var resetFocalLengthButtons: (([Int]) -> ())!
    var reselectFocalLengthButton: ((Int) -> ())!
    var removeFocalLengthButtons: (() -> ())!
    var removeSettingButtons: (() -> ())!
    var addSettingButton: ((String) -> ())!
    var addNewSettingButton: (() -> ())!
    var addEmptySettingButton: (() -> ())!
    var updateSettingButton: ((String) -> ())!
    var setEditButtonVisibilityAction: ((Bool) -> ())!
    var extendView: (() -> ())!
    var setData: ((String, Medium?) -> ())!
    
    init(actualSetting: ActualSetting) {
        settingsList = []
        mediumList = []
        
        persitanceManager = PersitanceManager.shared
        self.actualSetting = actualSetting
        
        super.init()
    }
    
    func setup() {
        populateMediumList()
        
        //createSettings()
        getSavedSettings()
        
        setupSettingsPickerView()
        setupMediumPicker()
    }
    
    func populateMediumList() {
        mediumList.append(Medium(height: 11*2.54, width: 14*2.54, andDescription: "11x14"))
        mediumList.append(Medium(height: 8*2.54, width: 10*2.54, andDescription: "8x10"))
        mediumList.append(Medium(height: 5*2.54, width: 7*2.54, andDescription: "5x7"))
        mediumList.append(Medium(height: 4*2.54, width: 5*2.54, andDescription: "4x5"))
        mediumList.append(Medium(height: 6, width: 12, andDescription: "6x12"))
        mediumList.append(Medium(height: 6, width: 9, andDescription: "6x9"))
        mediumList.append(Medium(height: 6, width: 8, andDescription: "6x8"))
        mediumList.append(Medium(height: 6, width: 7, andDescription: "6x7"))
        mediumList.append(Medium(height: 6, width: 6, andDescription: "6x6"))
        mediumList.append(Medium(height: 6, width: 4.5, andDescription: "6x4.5"))
        mediumList.append(Medium(height: 2.4, width: 3.6, andDescription: "35mm"))
    }
    
    func createSettings() {
        _ = createSetting(name: "RZ67", mediumID: -1)
        _ = createSetting(name: "645", mediumID: 9)
        _ = createSetting(name: "Hasselblad", mediumID: 8)
        _ = createSetting(name: "Nikon F6", mediumID: 10)
    }
    
    func getSavedSettings() {
        settingsList = persitanceManager.fetch()
        
        settingsList.forEach({
            if $0.mediumID == -1 {
                $0.medium = nil
            } else {
                $0.medium = mediumList[Int($0.mediumID)]
            }
        })
    }
    
    func setupMediumPicker() {
        var mediums = mediumList.filter({$0.type == .largeFormat})
        populatePicker("Large format:", mediums)
        
        mediums = mediumList.filter({$0.type == .mediumFormat})
        populatePicker("Medium format:", mediums)
        
        mediums = mediumList.filter({$0.type == .smallFormat})
        populatePicker("Small format:", mediums)
    }
    
    func setNewActualSetting(index: Int) {
        actualSetting.setCameraSetting(cameraSetting: settingsList[index])
        
        resetFocalLengthButtons(actualSetting.cameraSetting!.focalLengths)
    }
    
    func setNewActualFocalLength(index: Int) {
        actualSetting.setFocalLength(focalLengthIndex: index)
    }
    
    func addFocalLength(focalLength: Int) {
        actualSetting.cameraSetting!.addFocalLength(focalLength: focalLength)
        
        resetFocalLengthButtons(actualSetting.cameraSetting!.focalLengths)
        if let actualFocalLength = actualSetting?.actualFocalLength {
            reselectFocalLengthButton(actualFocalLength)
        }
    }
    
    func clearFocalLengthsView() {
        removeFocalLengthButtons()
    }
    
    func deleteActualSetting() {
        if let index = settingsList.firstIndex(where: {$0 == actualSetting!.cameraSetting}) {
            settingsList.remove(at: index)
            
            persitanceManager.delete(actualSetting.cameraSetting!)
            actualSetting.cameraSetting = nil

            removeSettingButtons()
            removeFocalLengthButtons()
            setupSettingsPickerView()
            
            actualSetting.reset()
        }
    }
    
    func setupSettingsPickerView() {
        for setting in settingsList {
            addSettingButton(setting.cameraName)
        }
        
        addNewSettingButton()
    }
    
    func createNewEmptySetting() {
        let newSetting = createSetting()
        
        settingsList.append(newSetting)
        
        addEmptySettingButton()
        setEditButtonVisibility(isHidden: true)
    }
    
    private func createSetting(name: String = "", mediumID: Int? = nil) -> SavedSetting {
        let setting = SavedSetting(context: persitanceManager.context)
        setting.cameraName = name
        setting.mediumID = Int16(mediumID ?? -1)
        
        persitanceManager.saveContext()
        
        return setting
    }
    
    func updateSetting(name: String, medium: Medium?) {
        actualSetting.cameraSetting?.cameraName = name
        actualSetting.cameraSetting?.medium = medium
        
        if medium == nil {
            actualSetting.cameraSetting?.mediumID = -1
        }
        else {
            actualSetting.cameraSetting?.mediumID = Int16(mediumList.firstIndex(of: medium!)!)
        }
        
        updateSettingButton(name)
        persitanceManager.saveContext()
    }
    
    func setEditButtonVisibility(isHidden: Bool = false) {
        setEditButtonVisibilityAction(isHidden)
    }
    
    func expandView() {
        extendView()
    }
    
    func setActualSettingData() {
        let name = actualSetting.cameraSetting?.cameraName ?? ""
        let medium = actualSetting.cameraSetting?.medium
        
        setData(name, medium)
    }
    
    func deleteFocalLength(index: Int) {
        let actualFocalLength = actualSetting.cameraSetting?.focalLengths[index]
        
        actualSetting.cameraSetting?.removeFocalLength(index: index)
        
        resetFocalLengthButtons(actualSetting.cameraSetting!.focalLengths)
        if let actualFocalLength = actualSetting?.actualFocalLength {
            reselectFocalLengthButton(actualFocalLength)
        }
        
        if (actualSetting.actualFocalLength == actualFocalLength) {
            actualSetting.actualFocalLength = nil
        }
    }
}
