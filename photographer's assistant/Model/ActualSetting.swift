//
//  ActualSetting.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 17.02.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class ActualSetting: NSObject {
    var cameraSetting: SavedSetting?
    var actualFocalLength: Int?
    var actualAperture: Aperture?

    func setCameraSetting(cameraSetting: SavedSetting) {
        self.cameraSetting = cameraSetting
        actualFocalLength = nil
    }
    
    func setFocalLength(focalLengthIndex: Int) {
        actualFocalLength = cameraSetting?.focalLengths[focalLengthIndex]
    }
    
    func reset() {
        cameraSetting = nil
        actualFocalLength = nil
        actualAperture = nil
    }
}
