//
//  ApertureToDistance.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 10.04.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class ApertureToDistanceCalculator: BaseCalculatorLogic {
    //var aperturePickerView: ApertureToDistanceController!
    var setCurrentAperture: ((Aperture) -> ())?
    var setSlider: ((Int) -> ())?
    
    func changeCurrentAperture(index: Int) {
        let currentAperture = apertures.getAperture(atIndex: index)
        
        actualSetting?.actualAperture = currentAperture
        setCurrentAperture?(currentAperture)
        calculateDepthOfField()
    }
    
    override func updateView(aperture: Aperture, and depth: Int) {
        let apertureIndex = apertures!.getAperturesIndex(aperture: aperture)
        setSlider?(apertureIndex)
        setCurrentAperture?(aperture)
    }
}
