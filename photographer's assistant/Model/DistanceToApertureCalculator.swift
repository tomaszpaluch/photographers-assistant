//
//  DistanceToApertureCalculator.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 10.04.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class DistanceToApertureCalculator: BaseCalculatorLogic {
    var setApertureResult: ((Aperture) -> ())!
    
    var actualShootingDepth: Int! {
        didSet {
            calculateDepthOfField()
        }
    }
    
    override func canCalculateDepthOfField() -> Bool {
        return actualSetting.actualFocalLength != nil &&
            actualShootingDepth != nil &&
            shootingDistanceInMeters != 0
    }
    
    override func setupAperture() {
        aperture = getAproximatedAperture()
    }
    
    private func getAproximatedAperture() -> Aperture?
    {
        let farSharpnessDistance = shootingDistanceInMeters + ((Double)(actualShootingDepth!))/2/100;
        
        let nominator = ((Double)(actualShootingDepth!)/200) * pow(focalLength,2);
        let denominator = (farSharpnessDistance * shootingDistanceInMeters * circleOfConfusion);
        
        let roughAperture = nominator/denominator;
        let finalAperture = apertures!.getNextClosesApertureTo(roughAperture: roughAperture)
        
        return finalAperture
    }
    
    override func proceedCalculatingDepthOfField() {
        let desiredDepthOfField = ((Decimal)(actualShootingDepth!))/100
        
        super.proceedCalculatingDepthOfField()
        
        var actualDepthOfField = depthBeforeObject + depthBehindObject
        
        while desiredDepthOfField > convertToDecimal(value: actualDepthOfField) {
            aperture = apertures!.getNextApertureTo(roughAperture: aperture!)
            setupHyperfocal()
            
            super.proceedCalculatingDepthOfField()
            let newActualDepthOfField = depthBeforeObject + depthBehindObject
            if (newActualDepthOfField < actualDepthOfField) || (newActualDepthOfField < 0) {
                break
            }
            
            actualDepthOfField = newActualDepthOfField
        }
        
        actualSetting.actualAperture = aperture
        setApertureResult(aperture!)
    }
    
    private func convertToDecimal(value: Double) -> Decimal {
        let description = String(format: "%0.2f", value)
        return Decimal(string: description)!
    }
}
