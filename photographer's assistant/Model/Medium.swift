//
//  Medium.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 06.02.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import Foundation

enum MediumType {
    case largeFormat
    case mediumFormat
    case smallFormat
}

class Medium: NSObject {
    var mediumDescription: String!
    var width, height: Double
    var type: MediumType!
    
    init(height: Double, width: Double, andDescription desc: String) {
        self.width = width
        self.height = height
        mediumDescription = desc
        
        super.init()
        
        self.setType()
    }
    
    func setType() {
        if (height > 6) {
            type = .largeFormat
        }
        else if (height == 6) {
            type = .mediumFormat
        }
        else {
            type = .smallFormat
        }
    }
    
    func calculateCropFactor() -> Double {
        let mediumDiagonal = getMediumsDiagonal()
        let referenceDiagonal = calculateDiagonal()
        
        return referenceDiagonal/mediumDiagonal
    }
    
    func getMediumsDiagonal() -> Double {
        return calculateDiagonal(width: width, andHeight: height)
    }
    
    func calculateDiagonal(width: Double = 3.6, andHeight height: Double = 2.4) -> Double {
        return sqrt(pow(width, 2) + pow(height, 2))
    }
}
