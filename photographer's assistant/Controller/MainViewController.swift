//
//  ViewController.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 04.02.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    var mainLogic: MainLogic
    let actualSetting: ActualSetting
    let apertures: Apertures
    
    @IBOutlet weak var focalLengthEquivalentLabel: UILabel!
    @IBOutlet weak var fieldOfViewLabel: UILabel!
    @IBOutlet weak var fieldOfDepthLabel: UILabel!
    @IBOutlet weak var fODBeforeObjectLabel: UILabel!
    @IBOutlet weak var fODBehindObjectLabel: UILabel!
    @IBOutlet weak var hyperfocalValueLabel: UILabel!
    
    @IBOutlet weak var distanceTextField: UITextField!
    
    @IBOutlet weak var auxiliaryView: UIView!
    
    @IBOutlet weak var settingViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var contentView: UIView!
    
    var auxiliaryControllers: [BaseCalculatorController]
    var actualAuxiliaryController: BaseCalculatorController!
    
    required init?(coder aDecoder: NSCoder) {
        auxiliaryControllers = []

        actualSetting = ActualSetting()
        apertures = Apertures()
        mainLogic = MainLogic(actualSetting: actualSetting, apertures: apertures)
        
        super.init(coder: aDecoder)
        
        mainLogic.setFocalLengthEquivalentLabel = {[unowned self] (value: Int?) -> Void in self.setFocalLengthEquivalentLabel(value: value)}
        mainLogic.setFieldOfLookingLabel = {[unowned self] (width: Double, height:Double) -> Void in self.setFieldOfLookingLabel(width: width, andHeight: height)}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addControlToDistanceTextFieldKeyboard()
        setupAuxiliaryControllers()
        
        auxiliaryView.addSubview(auxiliaryControllers[0].view)
        auxiliaryView.addSubview(auxiliaryControllers[1].view)
        auxiliaryView.bringSubviewToFront(auxiliaryControllers[0].view)
    }
    
    private func addControlToDistanceTextFieldKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        distanceTextField.inputAccessoryView = keyboardToolbar
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func setupAuxiliaryControllers() {
        let depthPriorityVC = DistanceToApertureController(nibName: "DistanceToApertureView", bundle: Bundle.main)
        let aperturePriorityVC = ApertureToDistanceController(nibName: "ApertureToDistanceView", bundle: Bundle.main)
        
        auxiliaryControllers.append(depthPriorityVC)
        auxiliaryControllers.append(aperturePriorityVC)
        
        actualAuxiliaryController = auxiliaryControllers[0]
        
        depthPriorityVC.viewController = self
        depthPriorityVC.actualSetting = actualSetting
        depthPriorityVC.apertures = apertures
        
        aperturePriorityVC.viewController = self
        aperturePriorityVC.actualSetting = actualSetting
        aperturePriorityVC.apertures = apertures
        
        depthPriorityVC.parallelAuxiliaryController = aperturePriorityVC
        aperturePriorityVC.parallelAuxiliaryController = depthPriorityVC
    }
    
    @IBAction func modeChange(_ sender: UISegmentedControl) {
        actualAuxiliaryController = auxiliaryControllers[sender.selectedSegmentIndex]
        auxiliaryView.bringSubviewToFront(actualAuxiliaryController.view)
    }
    
    @IBAction func changeDistanceValue(_ sender: UITextField) {
        let distance = getDistance()
        
        mainLogic.actualShootingDistance = distance //setDistance(distance: distance)
        
        auxiliaryControllers.forEach({$0.setActualShootingDistance(distance: distance)})
        actualAuxiliaryController.calculateDepthOfField()
    }
    
    private func getDistance() -> Int {
        if let distance = Int(distanceTextField.text ?? "") {
            return distance
        }
        return 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? SettingsViewController {
            destinationVC.viewController = self
            destinationVC.actualSetting = actualSetting
            
            destinationVC.contentView = contentView
            destinationVC.heightConstraint = settingViewHeightConstraint
        }
    }
    
    func getFrameWidth() -> CGFloat {
        return self.view.frame.size.width
    }
    
    func setFocalLengthEquivalentLabel(value: Int?) {
        var text: String
        
        if value == nil {
            text = "..."
        } else {
            text = String(describing: value!) + " mm"
        }
        
        focalLengthEquivalentLabel.text = text
    }
    
    func setFieldOfLookingLabel(width: Double, andHeight height: Double) {
        let text = String(format: "%.02f m x %.02f m", width, height)
        
        fieldOfViewLabel.text = text
    }
    
    func setCalculationValues(beforeObject: Double, behindObject: Double, andHyperfocal hyperfocal: Double) {
        let totalDepth = beforeObject + behindObject
        
        fieldOfDepthLabel.text = String(format: "%.02f m", totalDepth)
        fODBeforeObjectLabel.text = String(format: "%.02f m", beforeObject)
        fODBehindObjectLabel.text = String(format: "%.02f m", behindObject)
        hyperfocalValueLabel.text = String(format: "%.02f m", hyperfocal)
    }
    
    func makeCalculations() {
        mainLogic.makeCalculations()
        actualAuxiliaryController.calculateDepthOfField()
    }
}

