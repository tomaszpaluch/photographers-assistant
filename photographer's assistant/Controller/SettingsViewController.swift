//
//  SettingsViewController.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 13.02.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    var viewController: MainViewController!
    var settings: Settings!
    
    @IBOutlet weak var settingsContentView: UIView!
    @IBOutlet weak var focalLengthsContentView: UIView!

    @IBOutlet weak var deleteSettingButton: UIButton!
    @IBOutlet weak var saveSettingButton: UIButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mediumsSizeLabel: UILabel!
    @IBOutlet weak var focalLengthLabel: UILabel!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var focalLengthTextField: UITextField!
    
    @IBOutlet weak var mediumSizePicker: UIPickerView!
    
    var contentView: UIView!
    var heightConstraint: NSLayoutConstraint!
    
    var settingSelectorController : SettingSelectorController!
    var focalLengthSelectorController : FocalLengthSelectorController!
    
    var groupAControls: Array<UIView>
    var groupBControls: Array<UIView>
    
    var pickerData: [NSObject]
    
    var actualSetting: ActualSetting!
    
    required init?(coder aDecoder: NSCoder) {
        groupAControls = []
        groupBControls = []
        pickerData = []

        super.init(coder: aDecoder)
        
        
    }
    
    override func viewDidLoad() {
        populateGroupAControls()
        populateGroupBControls()
        addControlToNameTextFieldKeyboard()
        addControlToFocalLengthTextFieldKeyboard()

        settings = Settings(actualSetting: actualSetting)
        setupSettingsClosures()
        settings.setup()
        
        mediumSizePicker.delegate = self
        mediumSizePicker.dataSource = self
        
        groupAControls.forEach { $0.alpha = 0 }
    }
    
    func setupSettingsClosures() {
        settings.populatePicker = {[unowned self] (label:String, mediums: [Medium]) in self.populatePicker(label: label, mediums: mediums)}
        settings.resetFocalLengthButtons = {[unowned self] (focalLengths:[Int]) in self.resetFocalLengthButtons(focalLengths: focalLengths)}
        settings.reselectFocalLengthButton = {[unowned self] (focalLength: Int) in self.reselectFocalLengthButton(selectedFocalLength: focalLength)}
        settings.removeFocalLengthButtons = {[unowned self] in self.removeFocalLengthButtons()}
        settings.removeSettingButtons = {[unowned self] in self.removeSettingButtons()}
        settings.addSettingButton = {[unowned self] (cameraName: String) in self.addSettingButton(cameraName: cameraName)}
        settings.addNewSettingButton = {[unowned self] in self.addNewSettingButton()}
        settings.addEmptySettingButton = {[unowned self] in self.addEmptySettingButton()}
        settings.updateSettingButton = {[unowned self] (label: String) in self.updateSettingButton(label: label)}
        settings.setEditButtonVisibilityAction = {[unowned self] (isHidden:Bool) in self.setEditButtonVisibility(isHidden: isHidden)}
        settings.extendView = {[unowned self] in self.extendView()}
        settings.setData = {[unowned self]  (name:String, medium: Medium?) in self.setData(name: name, medium: medium)}
    }
    
    func populateGroupAControls() {
        groupAControls.append(nameLabel)
        groupAControls.append(mediumsSizeLabel)
        groupAControls.append(focalLengthLabel)
        groupAControls.append(nameTextField)
        groupAControls.append(focalLengthTextField)
        groupAControls.append(mediumSizePicker)
        groupAControls.append(deleteSettingButton)
        groupAControls.append(saveSettingButton)
    }
    
    func populateGroupBControls() {
        groupBControls.append(settingsContentView)
    }
    
    func addControlToNameTextFieldKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        nameTextField.inputAccessoryView = keyboardToolbar
    }
    
    func addControlToFocalLengthTextFieldKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let stopBarButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.dismissKeyboard))
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let addBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addFocalLength))
        keyboardToolbar.items = [stopBarButton, flexBarButton, addBarButton]
        focalLengthTextField.inputAccessoryView = keyboardToolbar
    }
    
    @objc func addFocalLength() {
        let focalLength = Int(focalLengthTextField.text ?? "nil")
        
        if (focalLength != nil && focalLength != 0) {
            settings.addFocalLength(focalLength: focalLength!)
        }
        
        focalLengthTextField.text = ""
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        dismissKeyboard()
        shrinkView()
        
        settings.setEditButtonVisibility()
        settings.clearFocalLengthsView()
        settings.deleteActualSetting()
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        dismissKeyboard()
        shrinkView()
        
        settings.updateSetting(name: nameTextField.text!, medium: getMediumFromPicker())
        settings.setEditButtonVisibility()
    }
    
    @objc func dismissKeyboard() {
        focalLengthTextField.text = ""
        view.endEditing(true)
    }
    
    private func shrinkView() {
        UIView.animate(withDuration: 1.0, animations: {
            var testRect: CGRect = self.contentView.frame
            testRect.size.height = 84;
            
            self.contentView.frame = testRect
            
            self.groupAControls.forEach { $0.alpha = 0 }
            self.groupBControls.forEach { $0.alpha = 1 }
            
            self.view.layoutIfNeeded()
        })
        
        heightConstraint.constant = 84
    }
    
    private func getMediumFromPicker() -> Medium? {
        let selectedRow = pickerData[mediumSizePicker.selectedRow(inComponent: 0)]
        
        if selectedRow is String {
            return nil
            
        } else {
            return (selectedRow as! Medium)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? SettingSelectorController {
            settingSelectorController = destinationVC
            destinationVC.settingsViewController = self
        }
        
        if let destinationVC = segue.destination as? FocalLengthSelectorController {
            focalLengthSelectorController = destinationVC
            destinationVC.settingsViewController = self
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        var title: String
        if (pickerData[row] is String) {
            title = pickerData[row] as! String
        } else {
            let medium = pickerData[row] as! Medium
            title = medium.mediumDescription!
        }
        
        return NSAttributedString(string: title, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var tempRow = row
        if (pickerData[row] is String) {
            tempRow += 1;
            pickerView.selectRow(tempRow, inComponent: component, animated: true)
        }
    }
    
    func populatePicker(label:String, mediums: [Medium])
    {
        pickerData.append(label as NSObject)
        pickerData.append(contentsOf: mediums)
    }
    
    func setData(name: String, medium: Medium?) {
        nameTextField.text = name
        
        var mediumSizePickerIndex = 0
        
        if let index = pickerData.firstIndex(where: { $0 == medium }) {
            mediumSizePickerIndex = index
        }
        
        mediumSizePicker.selectRow(mediumSizePickerIndex, inComponent: 0, animated: true)
    }
    
    func extendView() {
        UIView.animate(withDuration: 1.0, animations: {
            var testRect: CGRect = self.contentView.frame//self.contentView!.frame
            testRect.size.height = 282;
            self.contentView.frame = testRect
            
            self.groupAControls.forEach { $0.alpha = 1 }
            self.groupBControls.forEach { $0.alpha = 0 }
            
            self.view.layoutIfNeeded()
        })
        
        heightConstraint.constant = 282
    }
    
    func createNewEmptySetting() {
        settings.createNewEmptySetting()
        setActualSettingData()
    }
    
    func setActualSettingData() {
        settings.setActualSettingData()
        settings.expandView()
    }
    
    func setNewActualSetting(index: Int) {
        settings.setNewActualSetting(index: index)
    }
    
    func setNewActualFocalLength(index: Int) {
        settings.setNewActualFocalLength(index: index)
        
        viewController.makeCalculations()
    }
    
    func deleteFocalLength(index: Int) {
        settings.deleteFocalLength(index: index)
    }
    
    func addSettingButton(cameraName: String) {
        _ = settingSelectorController.addSettingsButton(cameraName: cameraName)
    }
    
    func addNewSettingButton() {
        settingSelectorController.addNewSettingButton()
    }
    
    func resetFocalLengthButtons(focalLengths: [Int]) {
        focalLengthSelectorController.resetButtons(focalLengths: focalLengths)
    }
    
    func reselectFocalLengthButton(selectedFocalLength: Int) {
        focalLengthSelectorController.reselectButton(selectedFocalLength: selectedFocalLength)
    }
    
    func removeFocalLengthButtons() {
        focalLengthSelectorController.removeButtons()
    }
    
    func addEmptySettingButton() {
        settingSelectorController.addEmptyButton()
    }
    
    func updateSettingButton(label: String) {
        settingSelectorController.updateButton(label: label)
    }
    
    func setEditButtonVisibility(isHidden: Bool) {
        focalLengthSelectorController.setEditButtonVisibility(isHidden: isHidden)
    }
    
    func removeSettingButtons() {
        settingSelectorController.removeButtons()
    }
}
