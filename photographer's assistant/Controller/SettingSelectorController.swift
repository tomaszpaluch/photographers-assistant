//
//  SettingsPickerView.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 16.02.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class SettingSelectorController: BaseSelectorController {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var settingsScrollConstraint: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        buttonWidth = 90
    }
    
    override func viewDidLoad() {
        baseContentView = contentView
        scrollConstraint = settingsScrollConstraint
        
        viewWidth = settingsViewController.viewController.getFrameWidth()
    }
    
    func addSettingsButton(cameraName: String = "") -> UIButton {
        let button = super.addButton(title: cameraName)
        
        return button
    }
    
    func addNewSettingButton() {
        super.addSpecialFunctionButton(title: "+")
    }
    
    @objc override func specialAction(sender: UIButton!) {
        settingsViewController.createNewEmptySetting()
    }
    
    func removeButtons() {
        for button in buttonList {
            button.removeFromSuperview()
        }
        
        buttonList.removeAll()
        
        specialButton.removeFromSuperview()
        
        actualXPosition = 0
        selectedButton = nil
        currentButtonIndex = 0
        scrollConstraint.constant = 0
    }
    
    func updateButton(label: String) {
        selectedButton!.setTitle(label, for: .normal)
    }
    
    func addEmptyButton() {
        let button = addSettingsButton()
        
        buttonAction(sender: button)
        selectedButton = button
        
        relocatePlusButton()
    }
    
    @objc override func buttonAction(sender: UIButton!) {
        super.buttonAction(sender: sender)
        
        settingsViewController.setNewActualSetting(index: sender.tag)
    }
    
    private func relocatePlusButton() {
        specialButton.frame.origin.x = CGFloat(actualXPosition)
    }
}
