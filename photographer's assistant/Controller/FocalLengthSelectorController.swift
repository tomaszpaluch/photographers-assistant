//
//  FocalLengthSelector.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 11.02.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class FocalLengthSelectorController: BaseSelectorController {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var focalLengthScrollConstraint: NSLayoutConstraint!
    
    var chooserMode: Bool = true
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        buttonWidth = 46
    }
    
    override func viewDidLoad() {
        scrollConstraint = focalLengthScrollConstraint
        baseContentView = contentView
        
        viewWidth = settingsViewController.viewController.getFrameWidth()
    }
    
    func resetButtons(focalLengths: [Int]) {
        self.removeButtons()
        
        for focalLength in focalLengths {
            addFocalLength(focalLength: focalLength)
        }
        
        if chooserMode {
            addEditButton()
        }
    }
    
    func removeButtons() {
        for button in buttonList {
            button.removeFromSuperview()
        }
        
        buttonList.removeAll()
        
        specialButton?.removeFromSuperview()
        
        actualXPosition = 0
        selectedButton = nil
        specialButton = nil
        currentButtonIndex = 0
        //scrollConstraint.constant = 0
    }
    
    func addFocalLength(focalLength: Int) {
        _ = addButton(title: "\(focalLength)")
    }
    
    @objc override func buttonAction(sender: UIButton!) {
        if chooserMode {
            super.buttonAction(sender: sender)
            settingsViewController.setNewActualFocalLength(index: sender.tag)
        } else {
            settingsViewController.deleteFocalLength(index: sender.tag)
        }
    }
    
    
    private func addEditButton() {
        addSpecialFunctionButton(title: "edit")
    }
    
    @objc override func specialAction(sender: UIButton!) {
        setEditButtonVisibility(isHidden: true)
        
        settingsViewController.setActualSettingData()
    }
    
    func reselectButton(selectedFocalLength: Int) {
        for button in buttonList {
            if (selectedFocalLength == Int(button.currentTitle!)!) {
                button.backgroundColor = selectedButtonColor
                selectedButton = button;
                break
            }
        }
    }
    
    func setEditButtonVisibility(isHidden: Bool) {
        if specialButton != nil {
            specialButton.isHidden = isHidden
            
        }
        else {
            addEditButton()
        }
        
        if isHidden {
            selectedButton?.backgroundColor = stdButtonColor
        }
        else {
            selectedButton?.backgroundColor = selectedButtonColor
        }
        
        chooserMode = !isHidden
    }
}
