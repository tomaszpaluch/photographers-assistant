//
//  CalculatorLogic.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 10.04.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class BaseCalculatorController: UIViewController {
    var viewController: MainViewController!
    weak var parallelAuxiliaryController: BaseCalculatorController!
    
    func setupBasicCalculatorLogicClosures(base: BaseCalculatorLogic) {
        base.setCalculationValues = {[unowned self] (beforeObject: Double, behindObject: Double, hyperfocal:Double) in self.setCalculationValues(beforeObject: beforeObject, behindObject: behindObject, andHyperfocal: hyperfocal)}
        base.updateSecondAuxiliaryView = {[unowned self] (aperture: Aperture, totalDepth: Int) in self.updateSecondAuxiliaryView(aperture: aperture, and: totalDepth)}
    }
    
    func updateView(aperture: Aperture,  depth: Int) {}
    
    func setActualShootingDistance(distance: Int) {}
    
    func calculateDepthOfField() {}
    
    func setCalculationValues(beforeObject: Double, behindObject: Double, andHyperfocal hyperfocal: Double) {
        viewController.setCalculationValues(beforeObject: beforeObject, behindObject: behindObject, andHyperfocal: hyperfocal)
    }
    
    func updateSecondAuxiliaryView(aperture: Aperture, and totalDepth: Int) {
        parallelAuxiliaryController.updateView(aperture: aperture, depth: totalDepth)
    }
}

