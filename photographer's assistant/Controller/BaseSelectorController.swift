//
//  PickerView.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 18.03.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class BaseSelectorController: UIViewController {
    var settingsViewController: SettingsViewController!
    var scrollConstraint: NSLayoutConstraint!
    
    var buttonList: [UIButton] = []
    var specialButton: UIButton!
    var selectedButton: UIButton?
    
    var baseContentView: UIView!
    
    let stdButtonColor = UIColor(red: 139/256, green: 99/256, blue: 103/256, alpha: 1)
    let selectedButtonColor = UIColor(red: 174/256, green: 142/256, blue: 105/256, alpha: 1)
    
    var buttonWidth = 0
    let freeSpace = 8
    var actualXPosition = 0
    var viewWidth: CGFloat = 0
    
    var currentButtonIndex = 0
    
    func addButton(title: String) -> UIButton {
        let button = UIButton(frame: CGRect(x: actualXPosition, y: 0, width: buttonWidth, height: 30))
        button.backgroundColor = stdButtonColor
        button.setTitle(title, for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.tag = currentButtonIndex
        
        buttonList.append(button)
        
        actualXPosition += buttonWidth + freeSpace
        currentButtonIndex += 1
        
        setNewConstraint()
        baseContentView.addSubview(button)
        
        return button
    }
    
    @objc func buttonAction(sender: UIButton!) {
        selectedButton?.backgroundColor = stdButtonColor
        sender.backgroundColor = selectedButtonColor
        
        selectedButton = sender;
    }
    
    func addSpecialFunctionButton(title: String) {
        specialButton = UIButton(frame: CGRect(x: actualXPosition, y: 0, width: buttonWidth, height: 30))
        specialButton.backgroundColor = stdButtonColor
        specialButton.setTitle(title, for: .normal)
        specialButton.addTarget(self, action: #selector(specialAction), for: .touchUpInside)
        
        buttonList.append(specialButton)
        
        setNewConstraint()
        baseContentView.addSubview(specialButton)
    }
    
    @objc func specialAction(sender: UIButton!) {}
    
    func setNewConstraint() {
        var constant = (CGFloat)(actualXPosition + 2 * freeSpace + buttonWidth) - viewWidth
        
        if (constant < 0) {
            constant = 0
        }
        
        scrollConstraint.constant = constant
    }
}


