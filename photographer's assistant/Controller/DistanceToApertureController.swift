//
//  ObjectDepthViewController.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 25.02.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class DistanceToApertureController: BaseCalculatorController {
    @IBOutlet weak var objectDepthTextField: UITextField!
    @IBOutlet weak var apertureResultLabel: UILabel!
    
    var distanceToApertureCalculator: DistanceToApertureCalculator!
    var actualSetting: ActualSetting!
    var apertures: Apertures!
    
    override func viewDidLoad() {
        distanceToApertureCalculator = DistanceToApertureCalculator(apertures: apertures, actualSetting: actualSetting)
        
        distanceToApertureCalculator.setApertureResult = {[unowned self] (aperture: Aperture) in self.setApertureResult(aperture: aperture)}
        setupBasicCalculatorLogicClosures(base: distanceToApertureCalculator)

        addControlToObjectDepthTextFieldKeyboard()
    }
    
    func addControlToObjectDepthTextFieldKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.dismissKeyboard))
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        objectDepthTextField.inputAccessoryView = keyboardToolbar
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func objectDepthTextFieldChanged(_ sender: UITextField) {
        if let depthValue = Int(objectDepthTextField.text!) {
            distanceToApertureCalculator.actualShootingDepth = depthValue 
        }
    }
    
    override func updateView(aperture: Aperture, depth: Int) {
        setApertureResult(aperture: aperture)
        setObjectDepthTextField(depth: depth)
    }
    
    func setApertureResult(aperture: Aperture) {
        let text = String(format: "f/%.1f", aperture)
        
        apertureResultLabel.text = text
    }
    
    private func setObjectDepthTextField(depth: Int) {
        objectDepthTextField.text = String(format: "%d", depth)
    }
    
    override func setActualShootingDistance(distance: Int) {
        distanceToApertureCalculator?.setActualShootingDistance(distance: distance)
    }
    
    override func calculateDepthOfField() {
        distanceToApertureCalculator?.calculateDepthOfField()
    }
}
