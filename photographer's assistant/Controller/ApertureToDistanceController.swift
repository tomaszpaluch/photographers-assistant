//
//  AperturePickerViewController.swift
//  photographer's assistant
//
//  Created by tomaszpaluch on 24.02.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class ApertureToDistanceController: BaseCalculatorController {
    @IBOutlet weak var apertureSlider: UISlider!
    
    @IBOutlet weak var minApertureLabel: UILabel!
    @IBOutlet weak var maxApertureLabel: UILabel!
    @IBOutlet weak var currentApertureLabel: UILabel!
    
    var apertureToDistanceCalculator: ApertureToDistanceCalculator!
    var apertures: Apertures!
    var actualSetting: ActualSetting!
    
    override func viewDidLoad() {
        apertureToDistanceCalculator = ApertureToDistanceCalculator(apertures: apertures, actualSetting: actualSetting)

        apertureToDistanceCalculator.setCurrentAperture = {[unowned self] (aperture: Aperture) in self.setCurrentAperture(currentAperture: aperture)}
        apertureToDistanceCalculator.setSlider = {[unowned self] (value: Int) in self.setSlider(value: value)}
        setupBasicCalculatorLogicClosures(base: apertureToDistanceCalculator)
        
        let aperturesCount = apertures.getAperturesCount()
        
        setupSlider(maxValue: aperturesCount)
        setSlider(value: aperturesCount/2)
        setCurrentAperture(currentAperture: apertures.getAperture(atIndex: aperturesCount/2))
        setLabels(minAperture: apertures.getMinAperture(), maxAperture: apertures.getMaxAperture())
    }
    
    func setupSlider(maxValue: Int) {
        let maxValueInFloat = (Float)(maxValue)
        apertureSlider.minimumValue = 0
        apertureSlider.maximumValue = maxValueInFloat - 1
        apertureSlider.value = maxValueInFloat/2
    }
    
    func setSlider(value: Int) {
        apertureSlider.value = (Float)(value)
    }
    
    func setCurrentAperture(currentAperture: Aperture) {
        currentApertureLabel.text = String(format: "f/%.1f", currentAperture)
    }
    
    func setLabels(minAperture: Aperture, maxAperture: Aperture) {
        minApertureLabel.text = String(format: "f/%.1f", minAperture)
        maxApertureLabel.text = String(format: "f/%.1f", maxAperture)
    }
    
    @IBAction func apertureValueChanged(_ sender: UISlider) {
        apertureToDistanceCalculator?.changeCurrentAperture(index: (Int)(sender.value))
        
        setCurrentAperture(currentAperture: apertures.getAperture(atIndex: (Int)(sender.value)))
    }
    
    override func updateView(aperture: Aperture, depth: Int) {
        setSlider(value: apertures.getAperturesIndex(aperture: aperture))
        setCurrentAperture(currentAperture: aperture)
    }
    
    override func setActualShootingDistance(distance: Int) {
        apertureToDistanceCalculator?.setActualShootingDistance(distance: distance)
    }
    
    override func calculateDepthOfField() {
        apertureToDistanceCalculator?.calculateDepthOfField()
    }
}
